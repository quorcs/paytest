package com.pay.test.service;

import com.pay.test.model.dto.AppRequest;

import java.math.BigDecimal;

/**
 * Интерфейс для работы с пользователями
 */
public interface UserService {

	/**
	 * Создание пользователя
	 */
	void createUser(AppRequest appRequest);

	/**
	 * Получение инфомрации о балансе пользователя
	 */
	BigDecimal getUserBalance(AppRequest appRequest);
}
