package com.pay.test.service;

import com.pay.test.dao.UserDao;
import com.pay.test.exceptions.InvalidPasswordException;
import com.pay.test.exceptions.UserAlreadyExistsException;
import com.pay.test.exceptions.UserNotFoundException;
import com.pay.test.model.dto.AppRequest;
import com.pay.test.model.entity.User;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * Реализация интерфейса для работы с пользователями
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	private final UserDao userDao;

	/**
	 * Создание пользователя
	 */
	@Override
	@Transactional
	public void createUser(AppRequest appRequest) {
		/*
		  Если пользователь не создался, выдаем ошибку, что пользователь с данным логином уже существует
		 */
		if (!userDao.createUser(appRequest)) {
			throw new UserAlreadyExistsException(appRequest.getLogin());
		}

		log.info("User created successfully: {}", appRequest.getLogin());
	}

	/**
	 * Получение инфомрации о балансе пользователя
	 */
	@Override
	@Transactional(readOnly = true)
	public BigDecimal getUserBalance(AppRequest appRequest) {
		User user = userDao.getUser(appRequest.getLogin());

		/*
		  Если пользователь не найден, то выбрасываем соответствующее исключение
		 */
		if (user == null) {
			throw new UserNotFoundException(appRequest.getLogin());
		}

		/*
		  Если заданный пароль не совпадает с реальным паролем пользователя, выдаем ошибку
		 */
		if (!user.getPassword().equals(appRequest.getPassword())) {
			throw new InvalidPasswordException(user.getLogin());
		}

		return user.getBalance();
	}
}
