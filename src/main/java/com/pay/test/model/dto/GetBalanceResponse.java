package com.pay.test.model.dto;

import com.pay.test.model.data.ResponseCodes;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.math.BigDecimal;

/**
 * DTO-класс ответа на запрос баланса пользователя
 */
@ToString
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "response")
public class GetBalanceResponse extends AppResponse {

	/**
	 * Баланс пользователя
	 */
	@XmlElement
	private BigDecimal balance;

	public GetBalanceResponse(BigDecimal balance) {
		super(ResponseCodes.ok.getCode());
		this.balance = balance;
	}
}