package com.pay.test.model.dto;

import lombok.*;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Запрос от клиента
 */
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement(name = "request")
public class AppRequest {
	/**
	 * Логин пользователя
	 */
	private String login;

	/**
	 * Пароль пользователя
	 */
	private String password;

	@XmlElement
	public String getLogin() {
		return login;
	}

	@XmlElement
	public String getPassword() {
		return password;
	}
}
