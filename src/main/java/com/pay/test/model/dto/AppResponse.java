package com.pay.test.model.dto;

import lombok.*;

import javax.xml.bind.annotation.*;

/**
 * Основной DTO-класс ответа на запросы
 */
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "response")
public class AppResponse {
	/**
	 * Код ответа {@link com.pay.test.model.data.ResponseCodes}
	 */
	@XmlElement(name = "result-code")
	private int code;
}
