package com.pay.test.model.data;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Перечисление всех возможных кодов ответов от сервера
 */
@Getter
@AllArgsConstructor
public enum ResponseCodes {

	/**
	 * Все ОК
	 */
	ok(0),

	/**
	 * Пользователь уже существует
	 */
	alreadyExists(1),

	/**
	 * Техническая ошибка
	 */
	commonError(2),

	/**
	 * Пользователь не найден
	 */
	userNotFound(3),

	/**
	 * Неправильный пароль
	 */
	invalidPassword(4);

	private final int code;
}
