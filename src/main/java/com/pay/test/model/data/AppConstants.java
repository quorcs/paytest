package com.pay.test.model.data;

/**
 * URL'ы эндпоинтов для {@link com.pay.test.controller.AppController}
 */
public class AppConstants {
	/**
	 * URL для эндпоинта на создание пользователя
	 */
	public static final String CREATE_USER = "/create";

	/**
	 * URL для эндпоинта для запроса баланса пользователя
	 */
	public static final String GET_BALANCE = "/balance";
}
