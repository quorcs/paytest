package com.pay.test.controller;

import com.pay.test.model.data.ResponseCodes;
import com.pay.test.model.dto.AppRequest;
import com.pay.test.model.dto.AppResponse;
import com.pay.test.model.dto.GetBalanceResponse;
import com.pay.test.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import static com.pay.test.model.data.AppConstants.CREATE_USER;
import static com.pay.test.model.data.AppConstants.GET_BALANCE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@Slf4j
@RestController
@RequiredArgsConstructor
public class AppController extends AppExceptionHandler {

	private final UserService userService;

	/**
	 * Регистрация пользователя с заданными логином и паролем
	 * Если пользователь уже существует, то на клиент вернется соответствующая ошибка
	 */
	@PostMapping(value = CREATE_USER, consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE)
	public AppResponse createUser(@RequestBody AppRequest appRequest) {
		log.info("Trying to create user: {}", appRequest.toString());
		userService.createUser(appRequest);
		return new AppResponse(ResponseCodes.ok.getCode());
	}

	/**
	 * Получение баланса пользователя с заданным логином и паролем
	 * Если пользователя не существует, или он ввел неверный пароль, то на клиент вернется соответствующая ошибка
	 */
	@GetMapping(value = GET_BALANCE, consumes = APPLICATION_XML_VALUE, produces = APPLICATION_XML_VALUE)
	public AppResponse getBalance(@RequestBody AppRequest appRequest) {
		log.info("Trying to get user balance: {}", appRequest.toString());
		return new GetBalanceResponse(userService.getUserBalance(appRequest));
	}
}
