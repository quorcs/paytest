package com.pay.test.controller;

import com.pay.test.exceptions.*;
import com.pay.test.model.data.ResponseCodes;
import com.pay.test.model.dto.AppResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Обработчик возможных ошибок
 */
@Slf4j
public class AppExceptionHandler {

	/**
	 * Обработка ошибки, если пользователь ввел неверный пароль
	 */
	@ExceptionHandler(InvalidPasswordException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody AppResponse invalidPasswordExceptionHandler(InvalidPasswordException ex) {
		log.info("Invalid password for user: {}", ex.getLogin());
		return new AppResponse(ResponseCodes.invalidPassword.getCode());
	}

	/**
	 * Обработка ошибки, если пользователя с заданным логином не существует
	 */
	@ExceptionHandler(UserNotFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody AppResponse userNotFoundExceptionHandler(UserNotFoundException ex) {
		log.info("User with login not exists: {}", ex.getLogin());
		return new AppResponse(ResponseCodes.userNotFound.getCode());
	}

	/**
	 * Обработка ошибки при регистрации, когда пользователь с заданным логином уже существует
	 */
	@ExceptionHandler(UserAlreadyExistsException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody AppResponse alreadyExistsUserExceptionHandler(UserAlreadyExistsException ex) {
		log.info("User with login already exists: {}", ex.getLogin());
		return new AppResponse(ResponseCodes.alreadyExists.getCode());
	}

	/**
	 * Обработка технической ошибки
	 */
	@ExceptionHandler(CommonException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody AppResponse commonExceptionHandler(CommonException ex) {
		log.info("Common exception");
		return new AppResponse(ResponseCodes.commonError.getCode());
	}

	/**
	 * Обработка глобальной ошибки
	 */
	@ExceptionHandler(Exception.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public @ResponseBody AppResponse exceptionHandler(Exception ex) {
		log.info("Internal error");
		return new AppResponse(ResponseCodes.commonError.getCode());
	}
}
