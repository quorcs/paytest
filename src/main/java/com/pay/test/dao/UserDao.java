package com.pay.test.dao;

import com.pay.test.model.dto.AppRequest;
import com.pay.test.model.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.*;

/**
 * DAO-класс для работы с базой данных
 */
@Component
@RequiredArgsConstructor
public class UserDao {

	private final DataSource dataSource;

	/**
	 * Создание записи о пользователе в базе данных
	 */
	public boolean createUser(AppRequest appRequest) {
		String sqlCreateUser = "INSERT INTO USERS(login, password) VALUES(?, ?);";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sqlCreateUser)) {
			statement.setString(1, appRequest.getLogin());
			statement.setString(2, appRequest.getPassword());
			return statement.executeUpdate() > 0;
		} catch (SQLException e) {
			return false;
		}
	}

	/**
	 * Получение из базы данных информации о пользователе
	 */
	public User getUser(String login) {
		String sqlCreateUser = "SELECT * FROM users where login = ?;";
		try (Connection connection = dataSource.getConnection(); PreparedStatement statement = connection.prepareStatement(sqlCreateUser)) {
			statement.setString(1, login);
			statement.execute();

			ResultSet resultSet = statement.getResultSet();
			if (resultSet.next()) {
				return new User(
						resultSet.getString("login"),
						resultSet.getString("password"),
						resultSet.getBigDecimal("balance")
				);
			}

			return null;
		} catch (SQLException e) {
			return null;
		}
	}
}
