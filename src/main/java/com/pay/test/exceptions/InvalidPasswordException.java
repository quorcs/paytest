package com.pay.test.exceptions;

/**
 * Ошибка о неверном пароле
 */
public class InvalidPasswordException extends CommonException {
	public InvalidPasswordException(String login) {
		super(login);
	}
}
