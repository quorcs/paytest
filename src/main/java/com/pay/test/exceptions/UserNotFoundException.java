package com.pay.test.exceptions;

/**
 * Ошибка при запросе баланса, что пользователя не существует
 */
public class UserNotFoundException extends CommonException {
	public UserNotFoundException(String login) {
		super(login);
	}
}
