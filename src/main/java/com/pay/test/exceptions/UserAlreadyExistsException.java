package com.pay.test.exceptions;

/**
 * Ошибка при регистрации, что пользователь уже существует
 */
public class UserAlreadyExistsException extends CommonException {
	public UserAlreadyExistsException(String login) {
		super(login);
	}
}
