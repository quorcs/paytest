package com.pay.test.exceptions;

import lombok.*;

/**
 * Техническая ошибка
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CommonException extends RuntimeException {
	/**
	 * Логин пользователя для отображения в логах
	 */
	private String login;
}
